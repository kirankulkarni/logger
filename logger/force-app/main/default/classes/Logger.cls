/**
* @author: Tquila dev
* @Used for: Log methods for api calls, method calls
* @TestClass: LoggerTest
* @History :
*/
global class Logger {
    
    global static list<Log__c> logs ;
    
    //Insert log dml
    global static void insertLog(){
        try{
            if(logs != null && !logs.isEmpty()){
                list<Log__c> logsToInsert = new list<Log__c>();
                Logger__c configLog = Logger__c.getInstance();
                if(configLog != null ){
                	for(Log__c e : logs){
                        if(e.EndPoint__c == null && configLog.Enabled__c){
							logsToInsert.add(e);
                        }else if(e.EndPoint__c != null && configLog.API_Logs_Enabled__c){
							logsToInsert.add(e);
                        }
                    }
                }
                if(!logsToInsert.isEmpty()){
                    insert logsToInsert;
                }
            }
        }
        catch(Exception exp){
            system.debug('Error in InsertExceptionlog method with Exception '+ exp.getMessage() + ' StackTrace: ' + exp.getStackTraceString());
        }
        finally{
			logs = new list<Log__c>();
        }
    }
    
    //create a non api log object
    global static void createLog(Logginglevel level,Exception ex,Exception module,String notes,HttpRequest req,HttpResponse res){
        Log__c e = Logger.createLogObject(level, ex, module, notes,req,res);
        if(logs == null){
            logs = new list<Log__c>();
        }
        logs.add(e);
    }
    
    //create an error log
    global static void createErrorLog(Exception ex,Exception module,String notes){
        Log__c e = Logger.createLogObject(LoggingLevel.ERROR, ex, module, notes,null,null);
        if(logs == null){
            logs = new list<Log__c>();
        }
        logs.add(e);
    }
    
    //construct error log object
    global static Log__c createLogObject(Logginglevel level,Exception ex,Exception module,String notes,HttpRequest req,HttpResponse res){
        try{
            Log__c  logObj = new Log__c();
            logObj.AdditionalInformation__c   = notes;
            logObj.AdditionalInformation__c   = logObj.AdditionalInformation__c.left(32768);
            logObj.Level__c                   = String.valueof(level);
            logObj.Module__c                  = (module != null) ? module.getStackTraceString().substringBefore(':') : ''; 
            if(ex != null){
                logObj.Cause__c               	  = (ex.getCause() != null) ? ex.getCause().getMessage() :'';
                logObj.LineNumber__c              = ex.getLineNumber();
                logObj.Message__c                 = ex.getMessage();
                logObj.StackTrace__c              = ex.getStackTraceString();
                logObj.Type__c                    = ex.getTypeName();
            }
            if(req != null){
                logObj.EndPoint__c  = '['+req.getMethod() +']'+req.getEndpoint().substringBeforeLast('/');
                logObj.Request__c   =  req.getBody() == '' ? req.getEndpoint().substringAfterLast('/') : req.getBody();
                logObj.Response__c 	= (res != null)? '['+res.getStatusCode().format() +']'+res.getBody(): '';
                logObj.Request__c 	= logObj.Request__c.left(32768);
                logObj.Response__c 	= logObj.Response__c.left(32768);    
            }
            return logObj;    
        }
        catch(Exception exp){
            system.debug('Error in Logging method with Exception '+ exp.getMessage() + ' StackTrace: ' + exp.getStackTraceString());
        }
        return null;
    }
}