/**
* @author: Tquila dev
* @Used for: generate test data for smarty grants related classes
* @TestClass: 
* @History : 
*/
@isTest
public class LoggerTest {

    public static testmethod void testinsertLog(){
		insert new Logger__c(API_Logs_Enabled__c = true,Enabled__c = true);
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('test/test/test');
        HttpResponse res = new HttpResponse();
        res.setBody('test');
        res.setStatusCode(200);
        Logger.createLog(LoggingLevel.INFO, null, new DmlException(), '',req,res );
        Logger.createErrorLog(new DmlException(), new DmlException(), '');
        system.assert(!Logger.logs.isEmpty());
        Logger.insertLog();
        system.assert(Logger.logs.isEmpty());
    }
    
    //exceptions
	public static testmethod void testinsertLogException(){
		insert new Logger__c(API_Logs_Enabled__c = true,Enabled__c = true);
        Logger.createErrorLog(new DmlException(), new DmlException(), '');
        system.assert(!Logger.logs.isEmpty());
        Logger.insertLog();
        system.assert(Logger.logs.isEmpty());
        
        Log__c e = Logger.createLogObject(null, null, null, null, null,null);
		system.assert(e == null);

        Logger.logs = null;
        Logger.insertLog();
        system.assert(Logger.logs != null);
    }
}